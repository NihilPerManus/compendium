# Compendium?! #

Based on the concept of "The Pippy Platform" which I developed early 2017 which was a DDD-centric model for centralising my applications.
Compendium will be an unbranded, CQRS-encouraging, centralizing framework which will allow developers to tailor the application to their needs.

The main difference other than the transistion from DDD to CQRS is that the plugins you create for Compendium are not executabled, but are DLL files.
This means they are lightweight, however requires Compendium to execute them.

The library Compenium uses is much more extensive than PippyPlatform and is very flexible. 
I'm yet to find an awesome use for it but there was less work put into this than PP. Not sure if it has something to do with knowing what I was doing this time around,
or the 50-odd interfaces I created to over-engineer this piece of work... Interface segregation Principle extreme.

Another awesome thing is that you can design your UI using the basic controls etc. And Compendium will override it using the UI frameworks in place: MahApps.Metro mixed with Material.Design which are open-source projects that I do not own.

  MahApps Metro: http://mahapps.com/
  Material Design: http://materialdesigninxaml.net/

### What is this repository for? ###

* Compendium - Yet another centralising app!
* Compendium Surface template - The template for quickly getting into coding. 
  * Note: Any breaking changes to the main app will come with a new template... However apps written in the previous template will require refactoring to work again.

### How do I get set up? ###

* Compile and run this application.
* A plugins folder will appear
* Go to the downloads section of this repository 
* Place file in "Documents/Visual Studio 20xx/Templates/Project Templates/Visual C#"
* Run the Developer command prompt for Visual studio (Not command prompt!)
* Run the following command: __devenv /installvstemplates__
* Create a new project using the template
* Open the CompositeRoot class and modify the Category and Name in the ImportedPage Attribute.
* Compile your project and place it in a folder in the plugins folder (Compendium will search ONLY in folders in the plugins folder. Don't place them directly in there)
* Run compendium (or restart it)
* on the left hand side click the "Applications" panel to revel your applications category. Open that then click the button with your app
* it should appear in the main panel, with the name of your application at the top



### Contribution guidelines ###

* A lot of the heavy lifting to read the assemblies and expose them to the application has been done.
* Shared functionality is done via the ComShared Library which is located in the repo in a folder called "ComShared"
  * reference the Library in the project (It has already been done but just in case)
  * Ensure that the namespaces are adhered to for consistency (It's Compendium.Shared.*   Not ComShared.*)
* Other than that, there is no real special requirement to modify this application

### Who do I talk to? ###

* The owner of the repository, me. Phillip Brown :D