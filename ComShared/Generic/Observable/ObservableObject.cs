﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Compendium.Shared.Generic.Observable
{
    //Inherit this class for a viewmodel class to give it the ability to tell the view "Hey! I've changed, now update your control!"
    public class ObservableObject : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        //This allows us to not specify a string and it will autoupdate the property of the setter it's used in. Magic stuff! Can't remember where I was reading it but the method of how it works is VERY technical
        protected void RaisePropertyChanged([CallerMemberName]string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
