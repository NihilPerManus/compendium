﻿using System;
using System.Windows.Input;

namespace Compendium.Shared.Generic.Relay
{
    /// <summary>
    /// This class aims to separate the execution logic from the object that is going to execute it.
    /// This allows you to have a command that multiple objects can use, and remove it without breaking errors.
    /// </summary>
    public class RelayCommand : ICommand
    {
        private readonly Action<object> _action; //A single relay instance should only have one command and it should not change

        public event EventHandler CanExecuteChanged = delegate { }; //ignore this. Explained later
        private string desc = "";

        public RelayCommand(Action<object> action, string desc = "")
        {
            _action = action; //store the action
        }

        public bool CanExecute(object parameter)
        {
            
            return true; //always allow this class to execute if told so. Logic should be external because it would be handled there anyway. 
        }

        public void Execute(object parameter)
        {
            if (parameter != null)
                Console.WriteLine("Parameter is not null");
            else
                Console.WriteLine("Parameter is null!!!");

            //Invoke the stored command :D

            Console.WriteLine($"Invoking action {desc}");
                _action.Invoke(parameter);
        }
    }
}
