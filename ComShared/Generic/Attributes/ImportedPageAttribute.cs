﻿using System;

namespace Compendium.Shared.Generic.Attributes
{
    [System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    sealed public class ImportedPageAttribute : Attribute
    {

        readonly string _name;
        readonly string _category;

        public ImportedPageAttribute(string name, string category)
        {
            _name = name;
            _category = category;
        }

        public string Name
        {
            get { return _name; }
        }
        public string Category { get { return _category; } }
        

   
    }
}
