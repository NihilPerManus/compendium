﻿

using Compendium.Shared.Generic.Queries;

namespace Compendium.Shared.Generic.Handlers
{
    //A query Handler has two things: 
    // * A query it needs to handle
    // * A return type for the result of the query
    public interface IQueryHandler<TQuery, out TReturn> where TQuery : IQuery
    {
        TReturn Handle(TQuery Query);
    }
}
