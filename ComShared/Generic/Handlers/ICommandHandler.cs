﻿using Compendium.Shared.Generic.Commands;

namespace Compendium.Shared.Generic.Handlers
{
    public interface ICommandHandler<TCommand> where TCommand : ICommand
    {
        void Execute(TCommand command);
    }
}
