﻿using Compendium.Shared.Generic.Properties;

namespace Compendium.Shared.Generic.Events
{
    public interface IEvent : IUnique
    {
    }
}
