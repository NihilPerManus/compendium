﻿using System.Reflection;
using System.Security;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace Compendium.Shared.Generic.MVVM.Behaviour
{
    /// <summary>
    /// Allows the binding of a secure string to the password field of a password box. Since the value is stored in a secure string it still maintains the level of security provided by SecureString
    /// </summary>
    public class PasswordBinderBehaviour : Behavior<PasswordBox>
    {
        //Create the dependency property (DP) that we will be using for the binding.
        //Here we are saying "Register a Dependency Property named "Password", the data type expected is a "SecureString" which will be maintained (owned) by this class"
        public static readonly DependencyProperty PasswordProperty = DependencyProperty.Register("Password", typeof(SecureString), typeof(PasswordBinderBehaviour), new PropertyMetadata(null));

        //Here is the public property that we will expose to MVVM.
        //GetValue and SetValue are methods which belong to the Behaviour<T> class (inherited) these methods will be talking to the behaviour
        public SecureString Password
        {
            get { return (SecureString)GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }

        //When we attach this to an object (Like a password property) we're going to raise an identical event which will inherently pass along it's data to it.
        //This will make the entire event system transparent to the developer. "AssociatedObject" is the same type as the type we have set in the Behaviour<T> which is why it has identical events
        protected override void OnAttached()
        {
            AssociatedObject.PasswordChanged += OnPasswordBoxValueChanged;
        }

        //This is how we are going to handle the password being changed
        private void OnPasswordBoxValueChanged(object sender, RoutedEventArgs e)
        {
            var binding = BindingOperations.GetBindingExpression(this, PasswordProperty); //Fetch the binding which is the "Password" property of this object

            //Check if it's not null before we proceed
            if (binding != null)
            {
                //retrieve the property from the type using reflection
                PropertyInfo property = binding.DataItem.GetType().GetProperty(binding.ParentBinding.Path.Path);
                if (property != null)
                {
                    //Set the property of the binding to the updated value of the Associated object
                    property.SetValue(binding.DataItem, AssociatedObject.SecurePassword, null);
                }
            }
        }
    }
}
