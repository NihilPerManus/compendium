﻿using Compendium.Shared.Generic.Repositories;

namespace Compendium.Shared.Generic.ReadModels
{
    public class EventRepositoryReadModel : IEventRepositoryReadModel
    {
        private IEventsRepository _eventRepo;

        public EventRepositoryReadModel(IEventsRepository eventrepo)
        {
            _eventRepo = eventrepo;
        }
    }

    public interface IEventRepositoryReadModel
    {
    }
}
