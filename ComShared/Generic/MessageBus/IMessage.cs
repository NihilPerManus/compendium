﻿using Compendium.Shared.Generic.Properties;
using System;

namespace Compendium.Shared.Generic.MessageBus
{
    public interface IMessage : IUnique
    {
        string Header { get; set; }
        string Message { get; set; }
        DateTime TimeStamp { get; }
    }
}