﻿using Compendium.Shared.Generic.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Compendium.Shared.Generic.MessageBus
{
    //The command message bus is a means for sending messages to other applications without having any awareness of them. It's similar to a central location where messages are stored and your class finds a message and just deals with it
    public class CommandMessageBus : ICommandMessageBus
    {
        //the cache of messages
        List<IMessage> messages = new List<IMessage>();

        //The event processor
        private EventHandlerList EventDelegateCollection = new EventHandlerList();

        //similar workings to a lock variable
        static readonly object CommandMessageKey = new object();

        //Registration and unregistration of a listener
        public event EventHandler<IMessage> MessageAdded
        {
            add
            {
                EventDelegateCollection.AddHandler(CommandMessageKey, value);
            }
            remove
            {
                EventDelegateCollection.RemoveHandler(CommandMessageKey, value);
            }
        }



        
        public void Create(IMessage entity)
        {
            if (entity != null)
            {
                //adds a message to the commandbus
                messages.Add(entity);

                //Get all the listeners subscribed to the messagebus 
                EventHandler<IMessage> notify = (EventHandler<IMessage>)EventDelegateCollection[CommandMessageKey];

                //Notify all listeners that a message has been added!
                notify?.Invoke(this, entity);
            }             
            else
                throw new NullValueMessageException();
        }

        public void Delete(IMessage entity)
        {
            //you don't delete a message.
        }

        public IMessage Read(int id)
        {
            //Finds a message by ID then returns it
            IMessage message = messages.Where(m => m.Id == id).FirstOrDefault();
            if (message != null)
                return message;
            else
                throw new MessageDoesNotExistException();

        }

        public IEnumerable<IMessage> ReadAll()
        {
            //give all the messages to the requester.
            return messages;
        }

        public void Update(int id, IMessage entity)
        {
            //you don't update  message
        }
    }

    public interface ICommandMessageBus : IRepository<IMessage,int>
    {
        event EventHandler<IMessage> MessageAdded;
        IEnumerable<IMessage> ReadAll();
    }
}
