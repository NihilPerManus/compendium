﻿using System;

namespace Compendium.Shared.Generic.MessageBus
{
    public class CommandMessage : IMessage
    {
        private DateTime _TimeStamp;
        private int _Id;
        private string _Header;
        private string _Message;

        public DateTime TimeStamp { get => _TimeStamp; set => _TimeStamp = value; }

        public int Id => _Id;

        public string Message { get => _Message; set => _Message = value; }
        public string Header { get => _Header; set => _Header = value; }

        public CommandMessage(string header, string message)
        {
            TimeStamp = DateTime.Now;
            Header = header;
            Message = message;
            _Id = GetHashCode();

            Console.WriteLine("========MESSAGE=========\n");
            Console.WriteLine($"Header: {Header}");
            Console.WriteLine($"Time: {TimeStamp}\n");
            Console.WriteLine($"Message: {Message}\n");
            Console.WriteLine("========================\n");

        }

    }
}
