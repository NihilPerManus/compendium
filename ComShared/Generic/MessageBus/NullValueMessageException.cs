﻿using System;
using System.Runtime.Serialization;

namespace Compendium.Shared.Generic.MessageBus
{
    [Serializable]
    internal class NullValueMessageException : Exception
    {
        public NullValueMessageException()
        {
        }

        public NullValueMessageException(string message) : base(message)
        {
        }

        public NullValueMessageException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NullValueMessageException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}