﻿using System;

namespace Compendium.Shared.Generic.Exceptions
{
    [Serializable]
    public class NotICompositeRootException : Exception
    {
        public NotICompositeRootException(string message) : base(message)
        {
        }
    }
}