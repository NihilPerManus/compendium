﻿using System;
using System.Runtime.Serialization;

namespace Compendium.Shared.Assemblies.Handlers
{
    [Serializable]
    public class NameNotFoundException : Exception
    {
        public NameNotFoundException()
        {
        }

        public NameNotFoundException(string message) : base(message)
        {
        }

        public NameNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NameNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}