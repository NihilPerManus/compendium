﻿namespace Compendium.Shared.Generic.EntryPoint
{
    public interface ICompositionRoot<T> where T : class
    {
        T GetInstance();
    }
}
