﻿using System.Windows.Controls;

namespace Compendium.Shared.Generic.EntryPoint
{
    public interface ISurfaceCompositionRoot : ICompositionRoot<Page>
    {
    }
}
