﻿namespace Compendium.Shared.Generic.EntryPoint
{
    //Attach this to the composition root to allow compendium to inject its dependencies into your application
    public interface IRegistersExternal
    {
        void Register<TInterface, TConcrete>(bool singleton = false)
            where TConcrete : class, TInterface
            where TInterface : class;
    }
}
