﻿namespace Compendium.Shared.Generic.Properties
{
    public interface IUnique
    {
        int Id { get; }
    }
}
