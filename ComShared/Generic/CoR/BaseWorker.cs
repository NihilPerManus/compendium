﻿using System;

namespace Compendium.Shared.Generic.CoR.Workers
{
    abstract public class BaseWorker : IWorker
    {
        //CanHandle is a function that takes an object in... processes it and spits out a yes/no whether it can be done or not
        private Func<object, bool> _CanHandle;
        public Func<object,bool> CanHandle { get { return _CanHandle; } set  { _CanHandle = value; } }

        //Who is the next object in the chain to try
        public IWorker Successor { get; set; }

        public BaseWorker()
        {
            

        }

        protected IWorker GetHandler(object taskData)
        {
            if (_CanHandle(taskData))
            {
                return this; //This class is capable of handling the task
            }
            else
            {
                if (Successor != null) //if it has a successor, check if it is capable of carrying out the task
                {
                    return Successor.GetCapableWorker(taskData); //Return what its successor thinks is can handle the task (This is recursive)
                }
                else
                {
                    return null; //end of the line and this class can't handle it. safe to say nothing here can handle it
                }
            }

        }

        public abstract void ExecuteTask(object taskData); //The task this class is supposed to perform on the passed object

        IWorker IWorker.GetCapableWorker(object taskData)
        {            
                return GetHandler(taskData);
        }
    }

    public interface IWorker
    {
        IWorker Successor { get; set; }

        IWorker GetCapableWorker(object taskData); //returns either this worker, its successor, or null if unable with no successor
        void ExecuteTask(object taskData);
    }
}
