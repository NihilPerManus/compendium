﻿namespace Compendium.Shared.Generic.CoR.Manager
{
    public interface IWorkerManager
    {
        void Execute(object workerData);
    }
}
