﻿using Compendium.Shared.Generic.Properties;

namespace Compendium.Shared.Generic.Repositories
{
    public interface IRepository<TEntity, TUnique> where TEntity : class, IUnique
    {
        void Create(TEntity entity);
        TEntity Read(TUnique id);
        void Update(TUnique id, TEntity entity);
        void Delete(TEntity entity);
    }
}
