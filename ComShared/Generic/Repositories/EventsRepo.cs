﻿using Compendium.Shared.Generic.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Compendium.Shared.Generic.Repositories
{
    //Repository for Events using the oldschool repository pattern.
    //Breaks CQRS (but not CQS) by having both read and write inthe same class. Not currently used but seems to be fully implemented. Not tested. Not commented
    public class EventsRepository : IEventsRepository 
    {
        List<IEvent> events = new List<IEvent>();

        public EventsRepository()
        {

        }

        public void Create(IEvent entity)
        {
            Console.WriteLine($"Created an event! => {entity}");
            if (!Exists(entity))
                events.Add(entity);
            else
                throw new EntityExistsException();
        }

        public void Delete(IEvent entity)
        {
            Console.WriteLine($"Deleted an event! => {entity}");
            if(Exists(entity))
            {
                events.Remove(Read(entity.Id));
            }
            else
            {
                throw new EntityDoesNotExistException();
            }
        }

        public IEvent Read(int id)
        {
            return events.Find(e => e.Id == id);
        }

        public void Update(int id, IEvent entity)
        {
            if(Exists(Read(id)))
            {
                var e = Read(id);
                e = entity; //Not sure if this updates list item, or just the local copy...
            }
            else
            {
                throw new EntityDoesNotExistException();
            }

        }

        private bool Exists(IEvent entity)
        {
            return events.FirstOrDefault(e => e.Id == entity.Id) != null; 
        }
    }

    public interface IEventsRepository : IRepository<IEvent, int>
    {
    }
}
