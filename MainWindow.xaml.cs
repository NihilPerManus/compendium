﻿using MahApps.Metro.Controls;
using System;
using System.Windows;

namespace Compendium
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow(IMainWindowViewModel vm)
        {
            InitializeComponent();

            DataContext = vm;


        }

        //Not sure why I have to do this now but the application doesn't seem to shutdown fully. I've probably stuffed something up somewhere...
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }

     
    }
}
