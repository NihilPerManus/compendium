﻿using Compendium.Internal.Assemblies;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Compendium.Internal.Handlers;
using Compendium.Internal.Queries;

namespace Compendium
{


    class MainWindowViewModel : Compendium.Shared.Generic.Observable.ObservableObject, IMainWindowViewModel
    {

        private ObservableCollection<IAssemblyCompositionRoot> _Assemblies = new ObservableCollection<IAssemblyCompositionRoot>();
        private IAssemblyCompositionRoot _CurrentView;
        private IRootViewDictionary _Views;
        private ISelectedViewQueryHandler _viewSelecter;
        private IRootViewDictionary _rootViewDictionary;

        public ObservableCollection<IAssemblyCompositionRoot> Assemblies { get { return _Assemblies; } set { _Assemblies = value; RaisePropertyChanged(); } }
        public IAssemblyCompositionRoot CurrentView { get { return _CurrentView; } set { _CurrentView = value; RaisePropertyChanged(); } }
        public IRootViewDictionary Views { get { return _Views; } set { _Views = value; RaisePropertyChanged(); } }
       
        //constructor.
        public MainWindowViewModel( 
            IRootViewDictionary rootViewDictionary,
            ISelectedViewQueryHandler viewSelecter            
            )
        {
            _rootViewDictionary = rootViewDictionary;
            _viewSelecter = viewSelecter;
        }

        //Get all the assemblies. This is in an Icommand and NOT the constructor so that the GUI can show itself before the thinking begins
        public ICommand GetAssemblies { get { return new Compendium.Shared.Generic.Relay.RelayCommand(e => _GetAssemblies()); } }

        //Shows a surface. This is handled via the Gui such as pressing a button or a view by means of passing a parameter in WPF
        public ICommand ShowSurface => new Shared.Generic.Relay.RelayCommand(v => _showView(v));

        //Shows the view
        private void _showView(object v)
        {
            Console.WriteLine($"_showView Called passing {v}");

           CurrentView = _viewSelecter.Handle(new SelectedViewQuery(v));
            
                Console.WriteLine($"Currently showing {CurrentView}");

        }

        //Transfers the views from the dictionary to the public property
        private void _GetAssemblies()
        {
            Views = _rootViewDictionary;            
        }
    }
}
