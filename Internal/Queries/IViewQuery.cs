﻿using Compendium.Shared.Generic.Queries;

namespace Compendium.Internal.Handlers
{
    public interface IViewQuery : IQuery
    {
    }
}