﻿using Compendium.Internal.Assemblies;

namespace Compendium.Internal.Handlers
{
    public interface ISelectedViewQuery : IViewQuery
    {
        IAssemblyCompositionRoot View { get; }
    }
}