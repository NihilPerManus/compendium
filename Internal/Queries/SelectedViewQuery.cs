﻿using Compendium.Internal.Handlers;
using Compendium.Internal.Assemblies;

namespace Compendium.Internal.Queries
{
    class SelectedViewQuery : ISelectedViewQuery
    {
        private IAssemblyCompositionRoot _View;

        public IAssemblyCompositionRoot View  => _View;

        public SelectedViewQuery(object view)
        {
            if (view is IAssemblyCompositionRoot _view)
            {
                //Set the view only if the object passed is a composition root object
                _View = _view;
            }
        }
    }
}
