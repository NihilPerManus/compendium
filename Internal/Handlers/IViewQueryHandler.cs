﻿using Compendium.Shared.Generic.Handlers;
using Compendium.Shared.Generic.Queries;

namespace Compendium.Internal.Handlers
{
    public interface IViewQueryHandler<TQuery, TReturn> : IQueryHandler<TQuery,TReturn> where TQuery : IQuery
    {
    }
}