﻿using Compendium.Internal.Assemblies;

namespace Compendium.Internal.Handlers
{
    public class SelectedViewQueryHandler : ISelectedViewQueryHandler
    {
        public IAssemblyCompositionRoot Handle(ISelectedViewQuery Query)
        {
            //return the selected View.... which was passed as a query...
            //CQRS can be soooooo verbose sometimes. Overly engineered for small projects such as this... but all in the name of learning
            return Query.View;
        }
    }

    public interface ISelectedViewQueryHandler : IViewQueryHandler<ISelectedViewQuery, IAssemblyCompositionRoot>
    {
    }
}
