﻿namespace Compendium.Internal.Handlers
{
    public interface IInternalCommandHandler<T> : ICommandHandler<T> where T : class
    {
        
    }
}