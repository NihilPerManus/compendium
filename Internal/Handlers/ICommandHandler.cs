﻿namespace Compendium.Internal.Handlers
{
    public interface ICommandHandler<T> where T : class
    {
        void Handle(T command);
    }
}