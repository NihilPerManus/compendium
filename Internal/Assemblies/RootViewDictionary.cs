﻿using System.Collections.Generic;

namespace Compendium.Internal.Assemblies
{
    //This is a dictionary of views for the GUI to get the values from
    public class RootViewDictionary : IRootViewDictionary
    {
        private Dictionary<string, IEnumerable<IAssemblyCompositionRoot>> _Dictionary;

        public Dictionary<string, IEnumerable<IAssemblyCompositionRoot>> Dictionary { get => _Dictionary; set => _Dictionary = value; }
    }

    public interface IRootViewDictionary
    {
        Dictionary<string, IEnumerable<IAssemblyCompositionRoot>> Dictionary { get; set; }
    }
}
