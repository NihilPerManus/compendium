﻿using System.IO;

namespace Compendium.Internal.Assemblies.Queries
{
    //Here is a query object. In order to query if there are DLLs I can use. The query should just be "What directory should I search?"
    public class AssemblyAvailibilityQuery : IAssemblyAvailibilityQuery
    {
        private DirectoryInfo _Directory;
        public DirectoryInfo Directory => _Directory; // I'm not really liking C#7's insistence on using lambdas for property getters...

        public AssemblyAvailibilityQuery(DirectoryInfo directory)
        {
            _Directory = directory;
        }
    }

    public interface IAssemblyAvailibilityQuery : IAssemblyQuery
    {
        DirectoryInfo Directory { get; }
    }
}
