﻿using Compendium.Shared.Generic.Queries;

namespace Compendium.Internal.Assemblies.Queries
{
    //Assembly queries!
    public interface IAssemblyQuery : IQuery
    {
    }
}
