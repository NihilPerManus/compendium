﻿using Compendium.Shared.Generic.EntryPoint;
using System.Collections.Generic;
using System.Windows.Controls;

namespace Compendium.Internal.Assemblies.Queries
{
    //Set up some Assembly compositions for them to be handled in a query
    public class AssemblyCompositionRootsQuery : IAssemblyCompositionRootsQuery
    {
        private readonly IEnumerable<ISurfaceCompositionRoot> _Roots;

        public IEnumerable<ISurfaceCompositionRoot> Roots => _Roots;

        public AssemblyCompositionRootsQuery(IEnumerable<ISurfaceCompositionRoot> roots)
        {
            _Roots = roots;
        }
    }

    public interface IAssemblyCompositionRootsQuery : IAssemblyQuery
    {
        IEnumerable<ISurfaceCompositionRoot> Roots { get; }
    }
}
