﻿using System.Collections.Generic;

namespace Compendium.Internal.Assemblies.Queries
{
    //Queries all the roots in this query to deterime their categories... Generally this means ALL the roots we've found
    public class AssemblyCategoryQuery : IAssemblyCategoryQuery 
    {
        private IEnumerable<IAssemblyCompositionRoot> _Roots;

        public IEnumerable<IAssemblyCompositionRoot> Roots{ get { return _Roots; }  }

        public AssemblyCategoryQuery(IEnumerable<IAssemblyCompositionRoot> roots)
        {
            //store a bunch of roots into this query to get handled
            _Roots = roots;
        }
    }

    public interface IAssemblyCategoryQuery : IAssemblyQuery
    {
        IEnumerable<IAssemblyCompositionRoot> Roots { get; }
    }
}
