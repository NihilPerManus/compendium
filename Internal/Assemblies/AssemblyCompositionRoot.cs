﻿using Compendium.Shared.Generic.Handlers;
using System.Windows.Controls;

namespace Compendium.Internal.Assemblies
{
    //This is a result item for the Query handler
    public class AssemblyCompositionRoot : IAssemblyCompositionRoot, IResult
    {
        public Page RootView { get; internal set; } //The resulting View from retrieving a composition root
        public string Name { get; internal set; } //The name retrieved from the attribute of the composition root

        public string Category { get; internal set; } //The category the result belongs to

        public AssemblyCompositionRoot(Page rootView, string category, string name)
        {          
            RootView = rootView;
            Name = name;
            Category = category;
        }

    }

    public interface IAssemblyCompositionRoot
    {
        Page RootView { get;  }
        string Name { get; }
        string Category { get; }
    }
}
