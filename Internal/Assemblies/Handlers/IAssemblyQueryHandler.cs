﻿using Compendium.Shared.Generic.Handlers;
using Compendium.Shared.Generic.Queries;

namespace Compendium.Internal.Assemblies.Handlers
{
    public interface IAssemblyQueryHandler<TQuery, out TReturn> : IQueryHandler<TQuery, TReturn> where TQuery : IQuery
    {
    }
}