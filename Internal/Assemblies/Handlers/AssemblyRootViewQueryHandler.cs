﻿using Compendium.Internal.Assemblies.Queries;
using Compendium.Shared.Assemblies.Handlers;
using Compendium.Shared.Generic.EntryPoint;
using Compendium.Shared.Generic.Exceptions;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Controls;

namespace Compendium.Internal.Assemblies.Handlers
{
    public class AssemblyRootViewQueryHandler : IAssemblyRootViewQueryHandler
    {
        public IEnumerable<IAssemblyCompositionRoot> Handle(IAssemblyCompositionRootsQuery Query)
        {
            List<IAssemblyCompositionRoot> rootViews = new List<IAssemblyCompositionRoot>();
            //Go through all the roots in our query
            foreach (ISurfaceCompositionRoot root in Query.Roots)
            {
                Page p = GetViewFromRoot(root);
                string n = GetNameFromRoot(root);
                string c = GetCategoryFromRoot(root);

                //Add a new Assembly composition based on what we were looking at
                rootViews.Add(new AssemblyCompositionRoot(p,c,n));
   
            }

            //In a sense it appears this class could just be a transformer class, it's merely transferring the type from one to another.
            return rootViews;
        }

        private string GetCategoryFromRoot(ISurfaceCompositionRoot root)
        {
            if (root.GetType().GetCustomAttribute(typeof(Compendium.Shared.Generic.Attributes.ImportedPageAttribute)) is Compendium.Shared.Generic.Attributes.ImportedPageAttribute attrib)
            {
                return attrib.Category;
            }
            throw new NameNotFoundException();
        }

        //Oh yeah, so at the top of a composition root you will find an attribute assigned to the class.
        //You change that to configure the settings of the page.
        //This function will get the name from the attribute.
        private string GetNameFromRoot(ISurfaceCompositionRoot root)
        {
            if (root.GetType().GetCustomAttribute(typeof(Compendium.Shared.Generic.Attributes.ImportedPageAttribute)) is Compendium.Shared.Generic.Attributes.ImportedPageAttribute attrib)
            {
                return attrib.Name;
            }
            throw new NameNotFoundException();
        }

        /// <summary>
        /// Returns the view that was set in the attribute of the compositionRoot
        /// </summary>
        /// <param name="type">The composition root object that was received</param>
        /// <returns>The page that is contained within the DLL based on what was defined in the attributes</returns>
        private Page GetViewFromRoot(ISurfaceCompositionRoot root)
        {

            if (root.GetType().GetCustomAttribute(typeof(Compendium.Shared.Generic.Attributes.ImportedPageAttribute)) is Compendium.Shared.Generic.Attributes.ImportedPageAttribute attrib)
            {

                    //breaks the container from here onwards. Injection must occur before here...
                    return root.GetInstance();

            }
            throw new ViewNotFoundException("View not found within the supplied composite root");
        }


    }

    public interface IAssemblyRootViewQueryHandler : IAssemblyQueryHandler<IAssemblyCompositionRootsQuery, IEnumerable<IAssemblyCompositionRoot>>
    {

    }


    
}
