﻿using Compendium.Internal.Assemblies.Queries;
using Compendium.Shared.Generic.Attributes;
using Compendium.Shared.Generic.EntryPoint;
using Compendium.Shared.Generic.Exceptions;

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Controls;


namespace Compendium.Internal.Assemblies.Handlers
{
    //This is a handler for the avaiblility queries. Pretty much it takes in a Query object which contains a directory.
    //It uses that directory and grabs all the DLL's it can find then cracks them open and sucks out the composition roots.
    public class AssemblyCompositionRootsQueryHandler : IAssemblyCompositionRootsQueryHandler
    {   
        public IEnumerable<ISurfaceCompositionRoot> Handle(IAssemblyAvailibilityQuery Query)
        {

            if (!Query.Directory.Exists) Query.Directory.Create();
            //create a list of roots
            List<ISurfaceCompositionRoot> roots = new List<ISurfaceCompositionRoot>();

            //Go through every directory inside the plugin directory for files which may contain (apps should only be one level deep)
            foreach (DirectoryInfo subDirectory in Query.Directory.GetDirectories())
            {
                Console.WriteLine($"Checking {subDirectory} for DLL files");
                //Go through every dll file and fid out which one is the one we're looking for
                foreach (FileInfo dllFile in subDirectory.GetFiles("*.dll"))
                {
                    var currentAssembly = Assembly.LoadFrom(dllFile.FullName);

                    Console.WriteLine($"  => Checking {dllFile} for composition root");
                    //go thorugh all the types contained in the Dll
                    foreach (Type type in currentAssembly.GetExportedTypes())
                    {
                        Console.Write($"    => Checking {type} to see if it is of {typeof(ISurfaceCompositionRoot)}: ");
                        //Is it a Surface Composition root?
                        if (IsCompositionRoot(type))
                        {
                            //This is one of the ones we're looking for. Add it to the roots! :D
                            Console.WriteLine($"[PASS]");
                            ISurfaceCompositionRoot root = Activator.CreateInstance(type) as ISurfaceCompositionRoot;
                            roots.Add(root);
                        }
                        else
                        {
                            //Nope, not this one :(
                            Console.WriteLine("[FAIL]");
                        }
                    }
                }
            }
            //Finally return all the roots we found
            return roots;
        }

        /// <summary>
        /// Returns whether or not a Type is a composition root or is inherited from it (the interface!)
        /// </summary>
        /// <param name="type">The type to check</param>
        /// <returns></returns>
        private bool IsCompositionRoot(Type type)
        {
            //Here is a new feature of .Net 7
            //Here we Check if a THING IS A TYPE OF THING and stores that type in a variable if so, otherwise returns false.
            //in this case attrib is the attribute but ONLY if it IS an importedPageAttribute
            if (type.GetCustomAttribute(typeof(ImportedPageAttribute)) is ImportedPageAttribute attrib)
            {
                //If that attribute is stuck to a composition root then it is confirmed
                if(typeof(ISurfaceCompositionRoot).IsAssignableFrom(type))
                {
                    return true;
                }
                else
                {
                    //Throw an error because we are using an attribute where it doesn't belong.
                    throw new NotICompositeRootException("ImportedPageAttribute must be assigned to class implementing the ICompositeRootInterface");
                }
            }
            //Return no. It's not a root.
            return false;
        }

  
    }



    //To compact the files that I'm using, I'm begining to stick Interfaces at the bottom pf a classes file ONLY if it is the same name with an I appended to the start.
    public interface IAssemblyCompositionRootsQueryHandler : IAssemblyQueryHandler<IAssemblyAvailibilityQuery, IEnumerable<ISurfaceCompositionRoot>>
    {
    }
}
