﻿using Compendium.Internal.Assemblies.Queries;
using System.Collections.Generic;
using System.Linq;


namespace Compendium.Internal.Assemblies.Handlers
{
    //Queries the Categories and all the composition roots that go with them
    class AssemblyCategoryQueryHandler : IAssemblyCategoryQueryHandler
    {
        public Dictionary<string, IEnumerable<IAssemblyCompositionRoot>> Handle(IAssemblyCategoryQuery Query)
        {

            Dictionary<string, IEnumerable<IAssemblyCompositionRoot>> dictionary = new Dictionary<string, IEnumerable<IAssemblyCompositionRoot>>();

            //Get all the different categories (No doubles)
            IEnumerable<string> categoryGroups = Query.Roots.Select(x => x.Category).Distinct();

            //For every category, attach all the composition roots that share the same category.
            categoryGroups.ToList()
                .ForEach(c => dictionary.Add(c, Query.Roots.Where(r => r.Category == c)));
                
            //return the dictionary
            return dictionary;
        }
    }

    internal interface IAssemblyCategoryQueryHandler : IAssemblyQueryHandler<IAssemblyCategoryQuery, Dictionary<string,IEnumerable<IAssemblyCompositionRoot>>>
    {
    }
}
