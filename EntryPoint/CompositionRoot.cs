﻿using System;
using System.Linq;

using SimpleInjector;
using Compendium.Internal.Assemblies.Handlers;
using Compendium.Internal.Handlers;
using Compendium.Internal.Assemblies.Queries;
using System.IO;
using Compendium.Shared.Generic.EntryPoint;
using Compendium.Internal.Assemblies;
using Compendium.Shared.Generic.Repositories;
using Compendium.Shared.Generic.MessageBus;
using System.Collections.Generic;

namespace Compendium.EntryPoint
{
    public class CompositionRoot : ICompendiumCompositionRoot
    {

        Container container = new Container();

        public CompositionRoot()
        {
            container.Register<MainWindow>(); //This is the compendium Window
            container.Register<IMainWindowViewModel, MainWindowViewModel>(); //This is the viewmodel for the window (contains all the stuff that the window will use)

            container.Register<ISelectedViewQueryHandler, SelectedViewQueryHandler>();


            //Get the composition roots from all the DLLs in our plugins folder
            IEnumerable<ISurfaceCompositionRoot> assemblies = (new AssemblyCompositionRootsQueryHandler()).Handle(new AssemblyAvailibilityQuery(new DirectoryInfo(Directory.GetCurrentDirectory() + @"\plugin")));

            //now we need to determine which ones accept external registrations
            assemblies?
                .Where(app => app is IRegistersExternal)? //Get all the assemblies that Register externally (That is, you can write an app that uses resources from that assembly and share results as if they are one data store)
                .Select(ext => ext as IRegistersExternal)? //Treat the collection as IRegisteredExternal since they are a list of apps that DO register externally
                .ToList()?
                .ForEach(r =>
                {
                    ExternalInjection(r); //Register external dependencies here :)
                });

            IEnumerable<IAssemblyCompositionRoot> compositionRootViews = (new AssemblyRootViewQueryHandler()).Handle(new AssemblyCompositionRootsQuery(assemblies));

            IAssemblyCategoryQuery categoryQuery = new AssemblyCategoryQuery(compositionRootViews);
            Dictionary<string, IEnumerable<IAssemblyCompositionRoot>> categories = (new AssemblyCategoryQueryHandler()).Handle(categoryQuery);

            categories.ToList().ForEach(entry => Console.WriteLine( entry.Key + " => " + entry.Value));

            container.Register<IRootViewDictionary>(() => new RootViewDictionary() { Dictionary = categories});
        }

        private void ExternalInjection(IRegistersExternal r)
        {
            r.Register<IEventsRepository, EventsRepository>(true);
            r.Register<ICommandMessageBus, CommandMessageBus>(true);
        }




        //This should not be modified unless you change DI from simpleInjector to something that uses a different approach
        //This function describes the ability to pull out a dependency from the registrations, and return it as itself or a derived type
        public MainWindow GetInstance() 
        {
            return container.GetInstance<MainWindow>();
        }
    }

    public interface ICompendiumCompositionRoot : ICompositionRoot<MainWindow>
    {
    }
}