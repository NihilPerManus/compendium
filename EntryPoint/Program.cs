﻿using System;

namespace Compendium.EntryPoint
{
    static class Program
    {
        
 
        //This is the entry point for the application as defined in Project > Compendium Properties > Application > Startup Object 
        [STAThread]
        static void Main()
        {
            //For some odd reason this needs to be declared before the mainwindow is generated... Otherwise the GUI framework dies... Order matters even if syntactically it doesn't matter.
            //You should be able to *create* any class in any order, as long as you *use* them in the correct order... but this causes framework issues... 
            //In my mind this makes ZERO sense...  But I think it has something to do with a static switch inside App. This is typical of Microsoft...

            App app = new App(); //Mainwindow doesn't use this *yet* but if you put this under the MainWindow's initialisation it screws up... try it...


            //Get the Datacontext of the object specified in the Composition Root
            MainWindow window = GetCompendiumMainWindow();

            //... place it here then rerun... Pffft


            app.Run(window); //run the compendium window. This has been built in the composition root to have all it's dependencies injected in.
       
        }


        private static MainWindow GetCompendiumMainWindow() => (new CompositionRoot()).GetInstance();

          
    }
}
